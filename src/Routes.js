import React from "react";
import {Route, Switch} from "react-router-dom";
import Layout from "./views/Layout"
import AddMovie from "./views/AddMovie";
import ListOfMovies from "./views/ListOfMovies";

const DefaultRoute = ({component: Component, ...rest}) => (
    <Route {...rest} render={(props) => (
        <Layout>
            <Component title={rest.title}  {...props} />
        </Layout>
    )}/>
);

function Routes() {
        return (
            <Switch>
                <DefaultRoute exact path="/"
                              title="List Of Movies" component={ListOfMovies}/>
                <DefaultRoute exact path="/add"
                              title="Add Movie" component={AddMovie}/>
            </Switch>
        );
}

export default  Routes;