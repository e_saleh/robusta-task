import React from "react"

const MovieCard = ({movie}) => {
    return(
        <div className="col-md-5 offset-md-1 offset-1 col-12 image-grid-item">
            <div className="image-grid-cover"></div>
            <div className="image-grid-content">
                <div className="content">
                    <span className="bolder">Movie Name :</span>
                    <span>{movie.movieName}</span>
                </div>
                <div className="content">
                    <span className="bolder">Year :</span>
                    <span>{movie.movieYear}</span>
                </div>
                <div className="content">
                    <span className="bolder">Movie Budget :</span>
                    <span>{movie.movieBudget + " " + movie.budgetCurrency}</span>
                </div>
            </div>
        </div>
    )
};
export default MovieCard