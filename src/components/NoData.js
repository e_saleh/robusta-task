import React from "react";
import "./../css/noData.scss";

const NoData = ({image}) => {

    return (
        <div className="no-data full-height full-width">
            <img src={require("./../images/" + image)} alt="no data" />
            <p className="capitalized">No Movies Yet</p>
        </div>
    )
}


export default NoData;