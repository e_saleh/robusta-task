import React from "react"
import SelectYears from "./SelectYears"

 const AddMovieUI = ({handleSubmission, handleInputChange}) => {
    return(
        <div id="add-movie">
            <div className="add-movie-area">
                <h3 className="heading">Add Movie</h3>
                <div className="row form">
                    <div className="col-md-12">
                        <form onSubmit={(e) => handleSubmission(e)}>
                            <div className="form-group">
                                <input type="text" name="movieName" required className="form-control"

                                       placeholder="Name *"
                                       onChange={(e) => handleInputChange(e)}/>
                            </div>
                            <div className="form-group">
                                <input type="number" required minLength="2" maxLength="15" min="10"
                                       step="0.001"
                                       name="movieBudget"
                                       className="form-control" placeholder="Your Budget *"
                                       onChange={(e) => handleInputChange(e)}/>
                            </div>
                            <div className="form-group">
                                <input type="text" name="budgetCurrency" required className="form-control"
                                       placeholder="currency *"
                                       pattern="^[a-zA-Z\s]*$"
                                       title="Accept only lower and uppercase letters from A-Z"
                                       onChange={(e) => handleInputChange(e)}/>
                            </div>
                            <div className="form-group">
                                <SelectYears handleInputChange={(e) => handleInputChange(e)}/>
                            </div>
                                <button type="submit"
                                        className="submit-btn pull-right">
                                    Add
                                </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
};
export default AddMovieUI