import React, {Component} from "react";
import {getAllYears} from "../../helpers/functions";

class SelectYears extends Component {

    render() {

        const year = getAllYears("2019", 100).map((year, index) => {
            return (
                <option key={index} value={year}>{year}</option>
            )
        });
        return (
            <select name="movieYear" className="form-control" onChange={(e) => this.props.handleInputChange(e)}>
                <option className="hidden" selected disabled>Please select movie year
                </option>
                {year}
            </select>
        )
    }
}

export default SelectYears