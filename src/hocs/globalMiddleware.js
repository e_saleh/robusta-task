import React from "react";

const globalMiddleware = WrappedComponent => {
    return (props) => {
        if (props.title && props.title !== "") {
            document.title =  props.title;
        } else{
            if (document.title === "")
                document.title = "Movies";
        }
        return (
            <WrappedComponent {...props} />
        )

    }
};

export default globalMiddleware;

