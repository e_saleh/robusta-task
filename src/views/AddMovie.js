import React, {Component} from "react";
import "../css/AddMovie.scss"
import AddMovieUI from "../components/add-movies/AddMovieUI";
import {globaleConstant} from "../globaleConstant";
import globalMiddleware from "../hocs/globalMiddleware";
import {showToast} from "../helpers/functions";

class AddMovie extends Component {
    componentDidMount() {
        let addMovie = document.getElementById('add-movie');
        addMovie.style.height = window.innerHeight + "px";
    }

    state = {
        movieName: "",
        movieBudget: 0,
        budgetCurrency: "",
        movieYear: 0,
        moviesArray: []
    };

    handleInputChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        });
    };
    handleSubmission = async (e) => {
        e.preventDefault();
        let movieObject = {
            "movieName": this.state.movieName,
            "movieBudget": this.state.movieBudget,
            "budgetCurrency": this.state.budgetCurrency,
            "movieYear": this.state.movieYear
        };
      await this.setState({
            moviesArray: [...this.state.moviesArray, movieObject]
        });

        localStorage.setItem(globaleConstant.movies, JSON.stringify(this.state.moviesArray));
        showToast("Movie Added Successfully", "green")
    };

    render() {
        return (
            <AddMovieUI handleSubmission={(e) => this.handleSubmission(e)}
                        handleInputChange={(e) => this.handleInputChange(e)}/>
        )
    }
}

export default globalMiddleware(AddMovie);