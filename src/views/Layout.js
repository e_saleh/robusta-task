import React, {useEffect} from "react"
import "../css/Layout.scss"
import {Link, withRouter} from "react-router-dom";

function Layout(props) {
    useEffect(() => {
        let layoutSidebar = document.getElementById('layout-sidebar');
        layoutSidebar.style.height = window.innerHeight + "px";
    });
    return (
        <div className="container-fluid" id="layout">
            <div className="row">
                <div id="layout-sidebar">
                    <img src="https://image.ibb.co/n7oTvU/logo_white.png" alt=""/>
                    <h3>Welcome</h3>
                    <p>You are 30 seconds away from build your own movies library</p>
                    <Link to={props.match.url === "/" ? "/add" : "/"} className="sidebar-btn">{props.match.url === "/" ? "Add Movie" : "Movies Library"}</Link>
                </div>

                <div id="layout-content">
                    {props.children}
                </div>
            </div>
        </div>
    );
}

export default withRouter(Layout);
