import React, {Component, Fragment} from "react";
import "../css/ListOfMovies.scss"
import {globaleConstant} from "../globaleConstant";
import MovieCard from "../components/list-of-movies/MovieCard";
import NoData from "../components/NoData";
import globalMiddleware from "../hocs/globalMiddleware";

class ListOfMovies extends Component {
    componentDidMount() {
        let listOfMovies = document.getElementById('list-of-movies');
        listOfMovies.style.height = window.innerHeight + "px";
    }
    render() {
        let moviesStore = JSON.parse(localStorage.getItem(globaleConstant.movies));
        const movies = moviesStore && moviesStore.length ? moviesStore.map((movie, index) => {
            return (
                <Fragment key={index}>
                    <MovieCard movie={movie}/>
                </Fragment>
            )
        }): <NoData image="no-movies.png"/>;
        return (
            <div id="list-of-movies">
                <div className="container">
                    <div className="row">
                        {movies}
                    </div>
                </div>
            </div>
        )
    }
}

export default globalMiddleware(ListOfMovies);