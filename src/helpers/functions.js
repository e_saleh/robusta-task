import izitoast from "izitoast";
import './../../node_modules/izitoast/dist/css/iziToast.min.css';

export function getAllYears( max, countOfYears) {
     let min = max - countOfYears;
    let years = [];
    for (let i = max; i >= min; i--) {
        years = [...years, i];
    }

    return years;
}
export const showToast = (msg, color) => {
    izitoast.show({
        color: color,
        title: "",
        message: msg,
        imageWidth: 50,
        layout: 1,
        balloon: false,
        close: true,
        rtl: false,
        position: 'bottomRight', // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter, center
        target: '',
        timeout: 5000,
        pauseOnHover: true,
        resetOnHover: false,
        progressBar: true,
        animateInside: true,
        transitionIn: 'fadeInUp',
        transitionOut: 'fadeOut',
        transitionInMobile: 'fadeInUp',
        transitionOutMobile: 'fadeOutDown',

    });
};